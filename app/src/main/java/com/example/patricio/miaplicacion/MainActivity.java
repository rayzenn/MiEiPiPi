package com.example.patricio.miaplicacion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.patricio.miaplicacion.vista.FormularioActivity;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private EditText editTextUsername, editPassword;
    private Button BtLogin;
    private TextView tvUsername, tvRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.editTextUsername = (EditText) findViewById(R.id.etUsername);
        this.editPassword = (EditText) findViewById(R.id.etPassword);
        this.tvRegistrar = (TextView) findViewById(R.id.tvRegistrar);


        this.BtLogin = (Button) findViewById(R.id.BtLogin);
        this.tvUsername = (TextView) findViewById(R.id.tvUsername);
        this.tvRegistrar = (TextView) findViewById(R.id.tvRegistrar);


        this.BtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Obtener el nombre de usuario
                String nombre_usuario = editTextUsername.getText().toString();

                // Mostrar el contenido en TextView
                tvUsername.setText(nombre_usuario);

                // Mostrar en Toast (mensaje temporal)
                Toast.makeText(getApplicationContext(),"Usernmame: " + nombre_usuario, Toast.LENGTH_SHORT).show();

                // Logs
                Log.v("HOLA MUNDO", "El usuario ingreso el nombre: " + nombre_usuario);
                Log.v("HOLA MUNDO", "El usuario ingreso el nombre: " + nombre_usuario);


            }
        });

        this.tvRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Iniciar la segunda ventana
                Intent nuevaVentana = new Intent(MainActivity.this, FormularioActivity.class);
                startActivity(nuevaVentana);


            }
        });
    }
}
