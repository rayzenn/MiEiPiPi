package com.example.patricio.miaplicacion.modelo.sqlite;

import android.provider.BaseColumns;

/**
 * Created by 19414383-4 on 25-08-2017.
 */

public class MiEiPipiDBContract {
    private MiEiPipiDBContract(){}

    public static class MiEiPiPuUsuarios implements BaseColumns {
        public static final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_PASSWORD = "password";
    }

}
