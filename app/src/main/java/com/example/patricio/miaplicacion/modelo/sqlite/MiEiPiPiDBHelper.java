package com.example.patricio.miaplicacion.modelo.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by 19414383-4 on 25-08-2017.
 */

public class MiEiPiPiDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MiEiPiPi.db";
    public static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE =
            "CREATE TABLE" + MiEiPipiDBContract.MiEiPiPuUsuarios.TABLE_NAME +
            "(" + MiEiPipiDBContract.MiEiPiPuUsuarios._ID + " INTEGER PRIMERA KEY,"
                    + MiEiPipiDBContract.MiEiPiPuUsuarios.COLUMN_NAME_USERNAME + " TEXT, " +
    MiEiPipiDBContract.MiEiPiPuUsuarios.COLUMN_NAME_PASSWORD + " TEXT)";

    public MiEiPiPiDBHelper(Context context) {
        super(context, this.DATABASE_NAME, null, this.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(this.SQL_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
