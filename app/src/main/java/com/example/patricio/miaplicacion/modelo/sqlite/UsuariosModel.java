package com.example.patricio.miaplicacion.modelo.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by 19414383-4 on 25-08-2017.
 */

public class UsuariosModel {
    private MiEiPiPiDBHelper dbHelper;

    public UsuariosModel(Context context){
        //instanciar dbhelpér
        this.dbHelper = new MiEiPiPiDBHelper(context);
    }
    public void crearUsuario(ContentValues usuario){
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        db.insert(MiEiPipiDBContract.MiEiPiPuUsuarios.TABLE_NAME, null, usuario);
    }
}
